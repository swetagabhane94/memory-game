const gameContainer = document.getElementById("game");
gameContainer.style.display = "none";

const startButton = document.getElementById("button-start");
startButton.addEventListener("click", startGame);

const score = document.querySelector(".score");
score.style.display = "none";

const gameEnd = document.querySelector(".game-end");
gameEnd.style.display = "none";

const restartButton = document.getElementById("button-restart");
restartButton.addEventListener("click", restartGame);
restartButton.style.display = "none";

const easyLevel = document.getElementById("easyButton");
easyLevel.addEventListener("click", easySelect);

const mediumLevel = document.getElementById("mediumButton");
mediumLevel.addEventListener("click", mediumSelect);

const hardLevel = document.getElementById("hardButton");
hardLevel.addEventListener("click", hardSelect);

let level = document.querySelector("#level");
level.style.display = "none";

var levelType;

let IMAGES = [];

function easySelect() {
  IMAGES = [
    "./gifs/1.gif",
    "./gifs/2.gif",
    "./gifs/3.gif",
    "./gifs/4.gif",
    "./gifs/5.gif",
    "./gifs/6.gif",
    "./gifs/1.gif",
    "./gifs/2.gif",
    "./gifs/3.gif",
    "./gifs/4.gif",
    "./gifs/5.gif",
    "./gifs/6.gif",
  ];
  levelType = "Easy";
  generateLevel();
}
// console.log('levelType', GLOBAL.levelType)
function mediumSelect() {
  IMAGES = [
    "./gifs/1.gif",
    "./gifs/2.gif",
    "./gifs/3.gif",
    "./gifs/4.gif",
    "./gifs/5.gif",
    "./gifs/6.gif",
    "./gifs/7.gif",
    "./gifs/8.gif",
    "./gifs/1.gif",
    "./gifs/2.gif",
    "./gifs/3.gif",
    "./gifs/4.gif",
    "./gifs/5.gif",
    "./gifs/6.gif",
    "./gifs/7.gif",
    "./gifs/8.gif",
  ];
  levelType = "Medium";
  generateLevel();
}

function hardSelect() {
  IMAGES = [
    "./gifs/1.gif",
    "./gifs/2.gif",
    "./gifs/3.gif",
    "./gifs/4.gif",
    "./gifs/5.gif",
    "./gifs/6.gif",
    "./gifs/7.gif",
    "./gifs/8.gif",
    "./gifs/9.gif",
    "./gifs/10.gif",
    "./gifs/11.gif",
    "./gifs/12.gif",
    "./gifs/1.gif",
    "./gifs/2.gif",
    "./gifs/3.gif",
    "./gifs/4.gif",
    "./gifs/5.gif",
    "./gifs/6.gif",
    "./gifs/7.gif",
    "./gifs/8.gif",
    "./gifs/9.gif",
    "./gifs/10.gif",
    "./gifs/11.gif",
    "./gifs/12.gif",
  ];
  levelType = "Hard";
  generateLevel();
}

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more

function shuffle(array) {
  let counter = array.length;
  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);
    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
let id = 1;

function createDivsForImages(imgArray) {
  gameLevelStart(imgArray);
  for (let image of imgArray) {
    // console.log(image)
    // create a new div
    let newDiv = document.createElement("div");
    newDiv.className = "memory-card";
    newDiv.setAttribute("data-image", image);

    newDiv.id = id;
    id++;

    let imgFaceUp = document.createElement("img");
    imgFaceUp.className = "face-up";
    imgFaceUp.src = image;

    let imgFaceDown = document.createElement("img");
    imgFaceDown.className = "face-down";
    imgFaceDown.src = "./gifs/front.png";

    // give it a class attribute for the value we are looping over
    newDiv.append(imgFaceUp);

    newDiv.appendChild(imgFaceDown);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

const highScore = document.getElementById("highScore");

let first, second;
let isFirstTime = false;
let input = [];
let scoreCount = 0;
// TODO: Implement this function!
function handleCardClick(event) {
  event.target.parentNode.classList.toggle("flip");
  scoreCount++;
  if (!isFirstTime) {
    isFirstTime = true;
    first = event.target.parentNode;
  } else {
    isFirstTime = false;
    second = event.target.parentNode;

    if (first.id == second.id) {
      scoreCount--;
      return;
    }

    stopEventListeners();

    if (first.dataset.image === second.dataset.image) {
      first.removeEventListener("click", handleCardClick);
      input.push(first.id);
      second.removeEventListener("click", handleCardClick);
      input.push(second.id);
      startEventListners();
    } else {
      setTimeout(() => {
        first.classList.remove("flip");
        second.classList.remove("flip");
        startEventListners();
      }, 1000);
    }
    isGameFinished();
  }
}

let highScoreEasy = localStorage.getItem("highScoreEasy");
let highScoreMedium = localStorage.getItem("highScoreMedium");
let highScoreHard = localStorage.getItem("highScoreHard");

function gameLevelStart(arr) {
  let level = arr.length;
  if (level === 12) {
    highScore.textContent = `High Score :- ${highScoreEasy}`;
  }
  if (level === 16) {
    highScore.textContent = `High Score :- ${highScoreMedium}`;
  }
  if (level === 24) {
    highScore.textContent = `High Score :- ${highScoreHard}`;
  }
}

function isGameFinished() {
  if (input.length === 12) {
    if (parseInt(highScoreEasy) > scoreCount) {
      localStorage.setItem("highScoreEasy", scoreCount);
      highScore.textContent = "High Score :- " + scoreCount;
    }
    gameEnd.style.display = "flex";
    setTimeout(() => {
      gameContainer.style.display = "none";
    }, 500);
  }

  if (input.length === 16) {
     if (parseInt(highScoreMedium) > scoreCount) {
      localStorage.setItem("highScoreMedium", scoreCount);
      highScore.textContent = "High Score :- " + scoreCount;
     }
     gameEnd.style.display = "flex";
     setTimeout(() => {
       gameContainer.style.display = "none";
     }, 500);
    }

  if (input.length === 24) {
    if (parseInt(highScoreHard) > scoreCount) {
      localStorage.setItem("highScoreHard", scoreCount);
      highScore.textContent = "High Score :- " + scoreCount;
    }
    gameEnd.style.display = "flex";
    setTimeout(() => {
      gameContainer.style.display = "none";
    }, 500);
  }
}

function stopEventListeners() {
  gameContainer.childNodes.forEach((element) => {
    element.removeEventListener("click", handleCardClick);
  });
}

let value = true;
function startEventListners() {
  gameContainer.childNodes.forEach((element) => {
    if (!value) {
      let classId = element.id;
      if (!input.includes(classId)) {
        element.addEventListener("click", handleCardClick);
      }
    } else {
      value = false;
    }
  });
}

function startGame() {
  startButton.style.display = "none";
  level.style.display = "block";
}

function generateLevel() {
  let shuffledImages = shuffle(IMAGES);
  createDivsForImages(shuffledImages, levelType);
  level.style.display = "none";
  restartButton.style.display = "block";
  gameContainer.style.display = "flex";
  score.style.display = "flex";
}

function restartGame() {
  location.reload();
}
